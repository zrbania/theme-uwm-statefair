<?php
/**
 * Template Name: Front-page
 *
 * Displays a full-width front page banner.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header();
get_sidebar( 'front-page-banner' ); ?>

<section id="landing">
	<div class="content-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="heading"><?php the_field( "landing_title" ); ?></h1>
				<div class="sub-heading"><?php the_field( "landing_date" ); ?></div>
			</div>

		</div>
	</div>
	<a href="<?php the_field( "button_url" ); ?>" class="button landing"><?php the_field( "landing_button_text" ); ?></a>
</section>
</div>

<div class="content-area">
<section id="overview">
	<div class="container">
		<div class="row">
				<h2><?php the_field( "section_1_title" ); ?></h2>
				<div class="col-md-12">
				<?php the_field( "intro_text" ); ?>
				</div>
		</div>


		<div id="highlights" class="row">
				<h4 style="margin-left: 20px;">Sponsorship Highlights</h4>
					<ul id="sponsorship">

					<?php

					$i = 1;

					// check if the repeater field has rows of data
					if( have_rows('sponsorship_highlights') ):

						// loop through the rows of data
						while ( have_rows('sponsorship_highlights') ) : the_row();
							if ($i <= 2) {
								echo '<div class="col-md-4">';
								echo '<li>';
								the_sub_field('highlight');
								echo '</li>';
								echo '</div>';
							} else if ($i > 2 && $i <= 4) {
								echo '<div class="col-md-4">';
								echo '<li>';
								the_sub_field('highlight');
								echo '</li>';
								echo '</div>';
							} else {
								echo '<div class="col-md-4">';
								echo '<li>';
								the_sub_field('highlight');
								echo '</li>';
								echo '</div>';
							}


						endwhile;

					else :

						// no rows found

					endif;

					?>
					</ul>
		</div>

		<!--<div class="row">
				<div class="col-md-6">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
				<div class="col-md-6">
						<blockquote cite="Jake, UWM Student">
							"Volunteering last year was so much fun! It was great being able to interact with prospective students."
						</blockquote>
						<i class="citation">- Jake, UWM Student</i>
			</div>
		</div>-->
	</div>
</section>
</div>

<div id="organization-image"> </div>

<div class="content-area">
<section id="organization">
	<div class="container">
		<div class="row">
			<h2><?php the_field( "section_2_title" ); ?></h2>
			<div class="col-md-6">
				<?php the_field( "section_2_content" ); ?>
			</div>
			<div class="col-md-6">
				<?php the_field( "section_2_quote" ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<span class="org-footnote" style="font-size: 0.90em;">
				<?php the_field( "section_2_footnotes" ); ?>
			</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php the_field( "section_2_button" ); ?>
			</div>
		</div>
	</div>
</section>
</div>

<div id="individual-image"> </div>

<div class="content-area">
<section id="individual">
	<div class="container">
		<div class="row">
			<h2><?php the_field( "section_3_title" ); ?></h2>
			<div class="col-md-6">
					<?php the_field( "section_3_content" ); ?>
			</div>
			<div class="col-md-6">
				<?php the_field( "section_3_quote" ); ?>
			</div>
		<div class="row">
			<div class="col-md-12">
				<?php the_field( "section_3_button" ); ?>
			</div>
		</div>
		</div>
	</div>
</section>
</div>

<div id="footer-image"> </div>

<?php get_footer(); ?>
