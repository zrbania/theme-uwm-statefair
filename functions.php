<?php

$host = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

if($host == 'wwwdev.uwm.edu/statefair/' || $host == 'wwwdev.uwm.edu/statefair' || $host == 'uwm.edu/statefair/' || $host == 'uwm.edu/statefair' || $host == 'www.uwm.edu/statefair/' || $host == 'www.uwm.edu/statefair')  {
    function statefair_scripts() {
        wp_enqueue_script( 'custom_js', get_template_directory_uri() . '-statefair/js/custom.js', array('jquery'), true );
    }

    add_action( 'wp_enqueue_scripts', 'statefair_scripts' );
} 
?>